//
//  Fitcoin_iOSApp.swift
//  Fitcoin-iOS
//
//  Created by Malcolm Anderson on 4/29/21.
//

import SwiftUI

@main
struct Fitcoin_iOSApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(FitcoinUserSession())
        }
    }
}
