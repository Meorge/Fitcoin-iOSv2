//
//  ServicesView.swift
//  Fitcoin-iOS
//
//  Created by Malcolm Anderson on 4/29/21.
//

import SwiftUI
import CodeScanner

struct ServicesView: View {
    @EnvironmentObject var userSession: FitcoinUserSession
    
    @State var scanQRModal: Bool = false
    @State var codeHasBeenScanned: Bool = false
    @State var scannedCode: String = "NOTHING"
    @State var linkRequestInfo: FitcoinLinkRequestInfo? = nil
    
    var body: some View {
        NavigationView {
            List {
                ForEach(userSession.services, id: \.service_id) { service in
                    ServiceRow(service: service)
                }
                .onDelete(perform: self.deleteServices)
            }
            .listStyle(InsetGroupedListStyle())
            .navigationTitle("Games")
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    Button(action: self.userSession.update) {
                        Label("Refresh", systemImage: "arrow.counterclockwise")
                    }
                }
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button(action: self.showScanView) {
                        Label("Add Game", systemImage: "plus")
                    }
                }
            }
            .sheet(isPresented: $scanQRModal) {
                ZStack {
                    CodeScannerView(codeTypes: [.qr]) { result in
                        switch result {
                        case .success(let data):
                            print("Data was scanned! \(data)")

                            if !data.starts(with: "LR_") {
                                print("Error - this isn't a valid link request QR code, since it doesn't start with LR_")
                                return
                            }
                            
                            let code = data.dropFirst(3)
                            print("Stripped code is \(code)")
                            scannedCode = String(code)
                            getInfoForLinkRequest()
                            
                            
                        case .failure(let error):
                            print("Error occurred: \(error.localizedDescription)")
                        }
                    }
                    .sheet(item: $linkRequestInfo, onDismiss: { self.linkRequestInfo = nil; self.scanQRModal = false }) { info in
                        VStack(alignment: .center) {
                            Spacer()
                            Text("\(info.service_name)")
                                .bold()
                                .font(.title)
                            Spacer()
                            Text("Do you want to allow this game to make Fitcoin purchases on your behalf?")
                                .multilineTextAlignment(.center)
                                .padding()
                            Text("You can remove this game from your list of approved games at any time by swiping on it in the Games tab.")
                                .font(.footnote)
                                .foregroundColor(.gray)
                                .multilineTextAlignment(.center)
                                .padding()
                            Spacer()
                            HStack {
                                Button(action: self.denyLinkRequest) {
                                    Text("Reject")
                                        .padding()
                                        .frame(maxWidth: .infinity)
                                        .foregroundColor(.white)
                                }
                                    .background(Color.red)
                                    .mask(RoundedRectangle(cornerRadius: 25.0))
                                
                                Button(action: self.approveLinkRequest) {
                                    Text("Accept")
                                        .padding()
                                        .frame(maxWidth: .infinity)
                                        .foregroundColor(.white)
                                    
                                }
                                    .background(Color.blue)
                                    .mask(RoundedRectangle(cornerRadius: 25.0))
                            }
                            .padding()
                        }
                    }
                    VStack {
                        Text("Please scan the Fitcoin QR code.")
                            .padding()
                            .shadow(radius: 5)
                        Spacer()
                    }
                }
            }


        }
    }
    
    func showScanView() {
        self.scanQRModal = true
    }
    
    func getInfoForLinkRequest() {
        userSession.lookupLinkRequest(requestID: scannedCode) { response in
            if response.error != nil {
                print("Error looking up link request: \(response.error!)")
                return
            }
            
            if response.response!.statusCode != 200 {
                print("Server-side error getting info for link request, code \(response.response!.statusCode): \(response.value!.message)")
                return
            }
            
            print("link request info is valid! \(response.value!.data!)")
            linkRequestInfo = response.value!.data!
        }
    }
    
    func approveLinkRequest() {
        respondToLinkRequest(action: .Approve)
    }
    
    func denyLinkRequest() {
        respondToLinkRequest(action: .Deny)
    }
    
    func respondToLinkRequest(action: FitcoinLinkResponseAction) {
        print("Respond to link request with \(action)")
        userSession.respondToLinkRequest(requestID: linkRequestInfo!.link_request_id, action: action) { response in
            if response.error != nil {
                print("Error responding to link request: \(response.error!)")
                return
            }
            
            if response.response!.statusCode != 200 {
                print("Server-side error responding to link request, code \(response.response!.statusCode): \(response.value!.message)")
                return
            }
            
            print("Responding was a success!")
            linkRequestInfo = nil
            scanQRModal = false
            userSession.update()
        }
    }
    
    func deleteServices(atIndices: IndexSet) {
        atIndices.forEach { index in
            // find the service this corresponds to
            let service = userSession.services[index]
            
            // get its id
            let id = service.service_id
            
            // pass that id to deleteService
            userSession.deleteService(serviceID: id) { response in
                if response.error != nil {
                    print("Error deleting service: \(response.error!)")
                    return
                }
                
                if response.response!.statusCode != 200 {
                    print("Server-side error deleting service, code \(response.response!.statusCode): \(response.value!.message)")
                }
                
                print("Deleting service was a success!")
                
                userSession.services.remove(at: index)
                userSession.update()
            }
        }
    }
}

struct ServiceRow: View {
    @State var service: FitcoinService
    var body: some View {
        VStack(alignment: .leading) {
            Text("\(service.service_name)")
                .font(.headline)
                .bold()
            Text("Added \(formattedDate)")
                .font(.footnote)
        }
    }
    
    var formattedDate: String {
        let d = DateFormatter()
        d.dateStyle = .short
        d.timeStyle = .short
        return d.string(from: service.date)
    }
}

struct ServicesView_Previews: PreviewProvider {
    static var previews: some View {
        ServicesView()
    }
}
