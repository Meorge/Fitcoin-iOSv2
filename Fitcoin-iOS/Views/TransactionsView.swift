//
//  TransactionsView.swift
//  Fitcoin-iOS
//
//  Created by Malcolm Anderson on 4/29/21.
//

import SwiftUI

struct TransactionsView: View {
    @EnvironmentObject var userSession: FitcoinUserSession
    var body: some View {
        NavigationView {
            List {
                ForEach(userSession.transactions, id: \.date) { transaction in
                    TransactionRow(transaction: transaction)
                }
            }
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    Button(action: self.userSession.update) {
                        Label("Refresh", systemImage: "arrow.counterclockwise")
                    }
                }
            }
            .listStyle(InsetGroupedListStyle())
            .navigationTitle("Transactions")
        }
    }
}

struct TransactionRow: View {
    @State var transaction: FitcoinTransaction
    
    var body: some View {
        HStack(spacing: 2) {
            VStack(alignment: .leading) {
                Text("\(transaction.service_name ?? "Deposit")")
                    .font(.headline)
                    .bold()
                Text("\(formattedDate)")
                    .font(.footnote)
            }
            Spacer()
            Text("\(transactionAmount)")
            Image(systemName: "heart.circle")
        }
    }
    
    var transactionAmount: String {
        let n = NumberFormatter()
        
        var prefix = ""
        if transaction.type == .Deposit {
            prefix = "+"
        } else {
            prefix = "-"
        }
        
        return prefix + (n.string(from: NSNumber(value: transaction.amount)) ?? "?")
    }
    
    var formattedDate: String {
        let d = DateFormatter()
        d.dateStyle = .short
        d.timeStyle = .short
        return d.string(from: transaction.date)
    }
}

struct TransactionsView_Previews: PreviewProvider {
    static var previews: some View {
        TransactionsView()
    }
}
