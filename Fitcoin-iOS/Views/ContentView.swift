//
//  ContentView.swift
//  Fitcoin-iOS
//
//  Created by Malcolm Anderson on 4/29/21.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var userSession: FitcoinUserSession
    @State var selectedTab: Int = 0
    var body: some View {
        TabView(selection: $selectedTab) {
            BalanceView()
                .tag(0)
                .tabItem { Label("Balance", systemImage: "heart.circle\(selectedTab == 0 ? ".fill" : "")") }
            TransactionsView()
                .tag(1)
                .tabItem { Label("Transactions", systemImage: "bag\(selectedTab == 1 ? ".fill" : "")") }
            ServicesView()
                .tag(2)
                .tabItem { Label("Games", systemImage: "gamecontroller\(selectedTab == 2 ? ".fill" : "")") }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(FitcoinUserSession())
    }
}
