//
//  UserRequests.swift
//  Fitcoin-iOS
//
//  Created by Malcolm Anderson on 4/29/21.
//

import Foundation
import Alamofire
import HealthKit

class FitcoinUserSession: ObservableObject {
//    let baseURL = "http://127.0.0.1:5000/api"
    let baseURL = "http://192.168.0.32:5000/api"
    let jsonDecoder = JSONDecoder()
    
    
    @Published var balance: Int = 0
    @Published var lastDeposit: Date = Date()
    @Published var transactions: [FitcoinTransaction] = []
    @Published var services: [FitcoinService] = []
    
    @Published var activeEnergyBurned: Double = -10.0
    @Published var eligibleFitcoinForActiveEnergy: Int = 0
    
    var login = FitcoinLoginCredentials(username: "firstCoolUser", password: "hunter2")
    
    
    // HealthKit
    private var healthStore: HKHealthStore = HKHealthStore()
    
    init() {
        initHealthKit()
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        jsonDecoder.dateDecodingStrategy = .formatted(df)
        update()
        
    }
    
    func initHealthKit() {
        let readTypes = Set([HKObjectType.quantityType(forIdentifier: .activeEnergyBurned)!])
        
        healthStore.requestAuthorization(toShare: nil, read: readTypes) { success, error in
            print("success = \(success), error = \(String(describing: error))")
        }
    }
    
    var authHeader: HTTPHeader {
        .authorization(username: login.username, password: login.password)
    }
    
    func update() {
        updateBalance()
        updateServices()
        updateTransactions()
        
        print("active energy is \(self.activeEnergyBurned)")
    }
    
    func updateBalance() {
        let url = baseURL + "/user/balance"
        
        let headers: HTTPHeaders = [authHeader]
        
        let request = AF.request(url, method: .get, parameters: nil, headers: headers)
        
        request.responseDecodable(of: FitcoinResult<FitcoinUserBalance>.self, decoder: jsonDecoder) { response in
            if response.error != nil {
                print("Error getting user balance: \(response.error!)")
                return
            }
            
            self.balance = response.value!.data!.balance
            self.lastDeposit = response.value!.data!.last_deposit
            
            self.updateHealthKit()
        }
    }
    
    func updateHealthKit() {
        // Update HealthKit information
        // Get active energy burned
        
        let activeEnergyType = HKSampleType.quantityType(forIdentifier: .activeEnergyBurned)!
        let start = self.lastDeposit
        let end = Date()
        
        let predicate = HKQuery.predicateForSamples(withStart: start, end: end, options: [])
        
        let activeEnergyQuery = HKStatisticsQuery(quantityType: activeEnergyType, quantitySamplePredicate: predicate, options: .cumulativeSum) { query, stats, error in
            let sumOfCalsRaw = stats?.sumQuantity()
            let sumOfCalsInUnits = sumOfCalsRaw?.doubleValue(for: HKUnit.largeCalorie())
            
            print("cals earned were \(String(describing: sumOfCalsInUnits))")
            
            DispatchQueue.main.sync {
                self.activeEnergyBurned = sumOfCalsInUnits ?? -1.0
                self.eligibleFitcoinForActiveEnergy = Int(sumOfCalsInUnits ?? 0) / 100
            }
        }
        
        healthStore.execute(activeEnergyQuery)
    }
    
    func updateServices() {
        let url = baseURL + "/user/approved_services"
        
        let headers: HTTPHeaders = [authHeader]
        
        let request = AF.request(url, method: .get, parameters: nil, headers: headers)
        
        request.responseDecodable(of: FitcoinResult<[FitcoinService]>.self, decoder: jsonDecoder) { response in
//            print(String(data: response.data!, encoding: .utf8)!)
            if response.error != nil {
                print("Error updating services: \(response.error!)")
                return
            }
            
            // Update the services
            self.services = response.value?.data ?? []
            
            print("servivces: \(self.services)")
        }
    }
    
    func updateTransactions() {
        let url = baseURL + "/user/transactions"
        let headers: HTTPHeaders = [authHeader]
        
        let request = AF.request(url, method: .get, headers: headers)
        
        request.responseDecodable(of: FitcoinResult<[FitcoinTransaction]>.self, decoder: jsonDecoder) { response in
            if response.error != nil {
                print("Error updating transactions: \(response.error!)")
                return
            }
            
            self.transactions = response.value?.data ?? []
            
            print("transactions: \(self.transactions)")
        }
    }
    
    func makeDeposit(amount: Int, handler: @escaping (AFDataResponse<FitcoinResult<Int>>) -> Void) {
        let url = baseURL + "/user/deposit"
        let request = AF.request(url, method: .post, parameters: ["amount" : amount], encoding: URLEncoding.queryString, headers: [authHeader])
        
        request.responseDecodable(of: FitcoinResult<Int>.self, decoder: jsonDecoder, completionHandler: handler)
    }
    
    func lookupLinkRequest(requestID: String, handler: @escaping (AFDataResponse<FitcoinResult<FitcoinLinkRequestInfo>>) -> Void) {
        let url = baseURL + "/service/link/about"
        let request = AF.request(url, method: .get, parameters: ["link_request_id" : requestID])
        
        request.responseDecodable(of: FitcoinResult<FitcoinLinkRequestInfo>.self, decoder: jsonDecoder, completionHandler: handler)
    }
    
    func respondToLinkRequest(requestID: String, action: FitcoinLinkResponseAction, handler: @escaping (AFDataResponse<FitcoinResultSimple>) -> Void) {
        let url = baseURL + "/service/link/respond"
        
        print("action is \(action.rawValue)")
        let request = AF.request(url, method: .post, parameters: ["link_request_id": requestID, "action" : action.rawValue], encoding: URLEncoding.queryString, headers: [authHeader])
        
        request.responseDecodable(of: FitcoinResultSimple.self, decoder: jsonDecoder, completionHandler: handler)
    }
    
    func deleteService(serviceID: String, handler: @escaping (AFDataResponse<FitcoinResultSimple>) -> Void) {
        let url = baseURL + "/user/remove_service"
        
        let request = AF.request(url, method: .post, parameters: ["service_id" : serviceID], encoding: URLEncoding.queryString, headers: [authHeader])
        
        request.responseDecodable(of: FitcoinResultSimple.self, decoder: jsonDecoder, completionHandler: handler)
    }
}

enum FitcoinLinkResponseAction: String {
    case Approve = "approve"
    case Deny = "deny"
}

struct FitcoinLoginCredentials {
    let username: String
    let password: String
}


struct FitcoinTransaction: Decodable {
    let type: TransactionType
    let amount: Int
    let service_id: String?
    let service_name: String?
    let date: Date
    
    enum TransactionType: String, CodingKey, Decodable {
        case Purchase = "purchase"
        case Deposit = "deposit"
    }
}

struct FitcoinLinkRequestInfo: Decodable, Identifiable {
    var id: String? { return link_request_id }
    
    let service_name: String
    let service_id: String
    let link_request_id: String
}

struct FitcoinService: Decodable {
    let service_name: String
    let date: Date
    let service_id: String
}

struct FitcoinResultSimple: Decodable {
    let message: String
}

struct FitcoinResult<T: Decodable>: Decodable {
    let message: String
    let data: T?
}

struct FitcoinUserBalance: Decodable {
    let balance: Int
    let last_deposit: Date
}
