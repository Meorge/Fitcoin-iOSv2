//
//  BalanceView.swift
//  Fitcoin-iOS
//
//  Created by Malcolm Anderson on 4/29/21.
//

import SwiftUI

struct BalanceView: View {
    @EnvironmentObject var userSession: FitcoinUserSession
    
    @State var toAdd: Int = 0
    var body: some View {
        NavigationView {
            List {
                Section {
                    HStack {
                        Spacer()
                        VStack {
                            Text("\(userSession.balance) Fitcoin")
                            Text("Updated \(userSession.lastDeposit)")
                        }
                        Spacer()
                    }
                }
                
                Section(header: Text("Debug")) {
                    Stepper("\(toAdd) Fitcoin", value: $toAdd, in: 0...100)
                }

                Section(header: Text("Ready to Deposit")) {
                    FitnessItemView(name: "Active Energy", rawQuantity: $userSession.activeEnergyBurned, eligibleFitcoin: $userSession.eligibleFitcoinForActiveEnergy, units: "cal", iconName: "flame.fill", color: .red)
                    
                    
                    Button(action: self.addToBalance) {
                        Label("Add to Balance", systemImage: "plus")
                    }
                }
            }
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    Button(action: self.userSession.update) {
                        Label("Refresh", systemImage: "arrow.counterclockwise")
                    }
                }
            }
            .navigationTitle("Balance")
            .listStyle(InsetGroupedListStyle())
        }
    }
    
    func addToBalance() {
        print("Add \(toAdd) to balance")
        
        userSession.makeDeposit(amount: toAdd) { response in
            if response.error != nil {
                print("Error adding to balance: \(response.error!)")
                return
            }
            
            if response.response!.statusCode != 200 {
                print("Server-side error adding to balance, code \(response.response!.statusCode): \(response.value!.message)")
                return
            }
            
            print("Adding to balance was success!")
            userSession.update()
        }
    }
}

struct FitnessItemView: View {
    @State var name: String
    @Binding var rawQuantity: Double
    @Binding var eligibleFitcoin: Int
    @State var units: String
    @State var iconName: String
    @State var color: Color
    
    var body: some View {
        HStack(spacing: 2) {
            HStack {
                Image(systemName: iconName).foregroundColor(color)
                    .frame(width: 25)
                VStack(alignment: .leading) {
                    Text(name)
                    Text("\(rawQuantity) \(units)")
                        .font(.subheadline)
                }
            }


            Spacer()
            
            Text("+ \(eligibleFitcoin)")
            Image(systemName: "heart.circle")
        }
    }
}

struct BalanceView_Previews: PreviewProvider {
    static var previews: some View {
        BalanceView()
    }
}
